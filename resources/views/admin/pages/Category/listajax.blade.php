<div class="card card-warning">
    <div class="card-header">
        <h3 class="card-title">Danh sách danh mục</h3>
    </div>
    <!-- /.card-header -->
    <div class="card-body">
        <div class="content-loai-san">
            <table class="table table-bordered">
                <tr>
                    <th>STT</th>
                    <th>Tên</th>
                    <th>Trạng thái</th>
                    <th>Hành động</th>
                </tr>
                <?php $stt=0 ?>
                @foreach($danhmuc as $item)
                <tr>
                <td>{{++$stt}}</td>
                <td>{{$item->name}}</td>
                <td>@if($item->status==1)
                {{"Hiện"}}
                @else {{"Ẩn"}}
                @endif
                </td>
                    <td><button type="button" class="btn btn-success btn-del-dm" id_dm="{{$item->id}}">Xóa</button> <button class="btn btn-warning btn-edit-dm" id_dm="{{$item->id}}">Sửa</button></td>
                </tr>
                @endforeach
            </table>
        </div>
    </div>
    <!-- /.card-body -->
</div>