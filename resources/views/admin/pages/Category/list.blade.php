@extends('admin.master')
@section('title','Quản lý danh mục')
@section('content')
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-4">
                    <div class="content-add">
                        <div class="card card-primary">
                            <div class="card-header">
                                <h3 class="card-title title_loaisan">Thêm danh mục</h3>
                            </div>
                            <!-- /.card-header -->
                            <!-- form start -->
                            <form method="Post"  role="form" id="danhmuc_form">

                                <div class="card-body">
                                    @csrf
                                    <div class="err"></div>
                                    <div class="form-group">
                                        <label>Tên danh mục</label>
                                        <input type="text" class="form-control" id="name" name="txt_name" placeholder="nhập tên danh mục">
                                    </div>

                                    <div class="form-group">
                                        <label >Trạng thái</label>
                                        <div id="status">
                                            <select name="slt_status" class="form-control">
                                                <option value="">--Chọn--</option>
                                                <option value="0">Ẩn</option>
                                                <option value="1">Hiển thị</option>
                                            </select>
                                        </div>
                                    </div>

                                </div>
                                <!-- /.card-body -->
                                <div class="card-footer">
                                    <button type="submit" class="btn btn-primary btn-add">Thêm</button>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="content-edit">
                        <div class="card card-primary">
                            <div class="card-header">
                                <h3 class="card-title">Sửa danh mục</h3>
                            </div>
                            <!-- /.card-header -->
                            <!-- form start -->
                            <form method="Post"  role="form1" id="suadanhmuc_form">

                                <div class="card-body">
                                    @csrf
                                    <div class="err"></div>
                                    <input type="hidden" class="form-control" name="id" id="txt_id" placeholder="nhập tên loại sân">
                                    <div class="form-group">
                                        <label>Tên</label>
                                        <input type="text" class="form-control" name="txt_name" id="txt_name" placeholder="nhập tên loại sân" >
                                    </div>
                                    <div class="form-group">
                                        <label >Trạng thái</label>
                                        <div class="status">

                                        </div>
                                    </div>

                                </div>
                                <!-- /.card-body -->
                                <div class="card-footer">
                                    <button type="submit" class="btn btn-primary">Sửa</button>
                                    <button type="button" class="btn btn-warning btn-huy">Hủy</button>
                                </div>
                            </form>
                        </div>

                    </div>
                </div>
                <div class="col-8">
                  <dic class="content-danhmuc">
                      <div class="card card-warning">
                          <div class="card-header">
                              <h3 class="card-title">Danh sách danh mục</h3>
                          </div>
                          <!-- /.card-header -->
                          <div class="card-body">
                              <div class="content-loai-san">
                                  <table class="table table-bordered">
                                      <tr>
                                          <th>STT</th>
                                          <th>Tên</th>
                                          <th>Trạng thái</th>
                                          <th>Hành động</th>
                                      </tr>
                                      <?php $stt=0 ?>
                                      @foreach($danhmuc as $item)
                                      <tr>
                                      <td>{{++$stt}}</td>
                                      <td>{{$item->name}}</td>
                                      <td>@if($item->status==1)
                                      {{"Hiện"}}
                                      @else {{"Ẩn"}}
                                      @endif
                                      </td>
                                      <td><button type="button" class="btn btn-success btn-del-dm" id_dm="{{$item->id}}">Xóa</button> <button class="btn btn-warning btn-edit-dm" id_dm="{{$item->id}}">Sửa</button></td>
                                      </tr>
                                      @endforeach
                                  </table>
                              </div>
                          </div>
                          <!-- /.card-body -->
                      </div>
                  </dic>
                    <!-- /.card -->
                </div>
                <!-- /.col -->
                <!-- /.col -->
            </div>
        </div>
    </section>

@endsection
