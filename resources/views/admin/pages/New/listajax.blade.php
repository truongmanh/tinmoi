<div class="card card-warning">
    <div class="card-header">
        <h3 class="card-title">Danh sách tin</h3>
    </div>
    <!-- /.card-header -->
    <div class="card-body">
        <div class="content-loai-san">
            <table class="table table-bordered">
                <tr>
                    <th>STT</th>
                    <th style="max-width: 200px">Tiêu đề</th>
                    <th>Hình ảnh</th>
                    <th>Hành động</th>
                </tr>
                <?php $stt=0 ?>
                @foreach($tin as $item)
                <tr>

                        <td>{{++$stt}}</td>
                        <td style="max-width: 200px">{{$item['title']}}</td>
                        <td><img src="{{$item['image']}}" alt="" width="100px"></td>
                    <td><a href="https://www.facebook.com/sharer/sharer.php?u={{$item['link']}}" class="social-button"><button class="btn btn-primary">FB</button></a>
                        <a href="https://plus.google.com/share?url={{$item['link']}}" class="social-button"><button class="btn btn-warning">G</button></a>
                    </td>
                </tr>
                @endforeach
            </table>
        </div>
    </div>
    <!-- /.card-body -->
</div>