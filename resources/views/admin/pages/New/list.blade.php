@extends('admin.master')
@section('title','Quản lý tin')
@section('content')
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-3">
                    <div class="content-add">
                        <div class="card card-primary">
                            <div class="card-header">
                                <h3 class="card-title title_loaisan">Lấy tin</h3>
                            </div>
                            <!-- /.card-header -->
                            <!-- form start -->
                            <form method="Post"  role="form" id="tin_form">

                                <div class="card-body">
                                    @csrf
                                    <div class="form-group">
                                        <label>Chọn danh mục</label>
                                        <select name="slt_cate" id="slt_cate" class="form-control">
                                            <option value="">--Chọn--</option>
                                            @foreach($cate as $item)
                                                <option value="{{$item->id}}">{{$item->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label >Chọn website</label>
                                        <div id="website">
                                            <select name="slt_site" class="form-control">
                                                <option value="">--Chọn--</option>
                                            </select>
                                        </div>
                                    </div>

                                </div>
                                <!-- /.card-body -->
                                <div class="card-footer">
                                    <button type="button" class="btn btn-primary btn-lay-tin">Lấy tin</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="col-9">
                    <dic class="content-site">
                        <div class="card card-warning">
                            <div class="card-header">
                                <h3 class="card-title">Danh sách tin</h3>
                            </div>
                            <!-- /.card-header -->
                            <div class="card-body">
                                <div class="content-loai-san">
                                    <table class="table table-bordered">
                                        <tr>
                                            <th>STT</th>
                                            <th style="max-width: 200px">Tiêu đề</th>
                                            <th>Hình ảnh</th>
                                            <th>Hành động</th>
                                        </tr>
                                        <?php $stt=0 ?>
                                        @foreach($tin as $item)
                                            <tr>

                                                <td>{{++$stt}}</td>
                                                <td style="max-width: 200px">{{$item->title}}</td>
                                                <td><img src="{{$item->image}}" alt="" width="100px"></td>
                                                <td><a href="https://www.facebook.com/sharer/sharer.php?u={{$item->link}}" class="social-button"><button class="btn btn-primary">FB</button></a>
                                                    <a href="https://plus.google.com/share?url={{$item->link}}" class="social-button"><button class="btn btn-warning">G</button></a>
                                                </td>
                                            </tr>
                                        @endforeach
                                    </table>
                                </div>
                                {{$tin->links()}}
                            </div>
                            <!-- /.card-body -->
                        </div>
                    </dic>
                    <!-- /.card -->
                </div>
                <!-- /.col -->
                <!-- /.col -->
            </div>
        </div>
    </section>

@endsection
