<div class="card card-warning">
    <div class="card-header">
        <h3 class="card-title">Danh sách user</h3>
    </div>
    <!-- /.card-header -->
    <div class="card-body">
        <div class="content-loai-san">
            <table class="table table-bordered">
                <tr>
                    <th>STT</th>
                    <th>Tên</th>
                    <th>Email</th>
                    <th>Hành động</th>
                </tr>
                <?php $stt=0 ?>
                @foreach($user as $item)
                <tr>
                <td>{{++$stt}}</td>
                <td>{{$item->name}}</td>
                <td>{{$item->email}}
                </td>
                <td><button type="button" class="btn btn-success btn-del-dm" id_user="{{$item->id}}">Xóa</button></td>
                </tr>
                @endforeach
            </table>
        </div>
    </div>
    <!-- /.card-body -->
</div>