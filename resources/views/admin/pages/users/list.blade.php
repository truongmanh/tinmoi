@extends('admin.master')
@section('title','Quản lý danh mục')
@section('content')
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-4">
                    <div class="content-add">
                        <div class="card card-primary">
                            <div class="card-header">
                                <h3 class="card-title title_loaisan">Thêm user</h3>
                            </div>
                            <!-- /.card-header -->
                            <!-- form start -->
                            <form method="Post"  role="form" id="user_form">

                                <div class="card-body">
                                    @csrf
                                    <div class="err"></div>
                                    <div class="form-group">
                                        <label>Tên</label>
                                        <input type="text" class="form-control"  name="txt_name" placeholder="nhập tên danh mục">
                                    </div>
                                    <div class="form-group">
                                        <label>Email</label>
                                        <input type="text" class="form-control"  name="txt_email" placeholder="nhập email">
                                    </div>
                                    <div class="form-group">
                                        <label>Mật khẩu</label>
                                        <input type="password" class="form-control"  name="txt_mk" placeholder="nhập mật khẩu">
                                    </div>

                                </div>
                                <!-- /.card-body -->
                                <div class="card-footer">
                                    <button type="submit" class="btn btn-primary btn-add">Thêm</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="col-8">
                    <dic class="content-user">
                        <div class="card card-warning">
                            <div class="card-header">
                                <h3 class="card-title">Danh sách user</h3>
                            </div>
                            <!-- /.card-header -->
                            <div class="card-body">
                                <div class="content-loai-san">
                                    <table class="table table-bordered">
                                        <tr>
                                            <th>STT</th>
                                            <th>Tên</th>
                                            <th>Email</th>
                                            <th>Hành động</th>
                                        </tr>
                                        <?php $stt=0 ?>
                                        @foreach($user as $item)
                                            <tr>
                                                <td>{{++$stt}}</td>
                                                <td>{{$item->name}}</td>
                                                <td>{{$item->email}}
                                                </td>
                                                <td><button type="button" class="btn btn-success btn-del-user" id_user="{{$item->id}}">Xóa</button></td>
                                            </tr>
                                        @endforeach
                                    </table>
                                </div>
                            </div>
                            <!-- /.card-body -->
                        </div>
                    </dic>
                    <!-- /.card -->
                </div>
                <!-- /.col -->
                <!-- /.col -->
            </div>
        </div>
    </section>

@endsection
