@extends('admin.master')
@section('title','Quản lý website')
@section('content')
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-3">
                    <div class="content-add">
                        <div class="card card-primary">
                            <div class="card-header">
                                <h3 class="card-title title_loaisan">Thêm website</h3>
                            </div>
                            <!-- /.card-header -->
                            <!-- form start -->
                            <form method="Post"  role="form" id="site_form">

                                <div class="card-body">
                                    @csrf
                                    <div class="err"></div>
                                    <div class="form-group">
                                        <label>Tên website</label>
                                        <input type="text" class="form-control"  name="txt_name" placeholder="nhập tên danh mục">
                                    </div>
                                    <div class="form-group">
                                        <label>Danh mục</label>
                                        <select name="slt_cate" class="form-control">
                                            <option value="">--Chọn--</option>
                                           @foreach($cate as $item)
                                            <option value="{{$item->id}}">{{$item->name}}</option>
                                               @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label>Đường dẫn</label>
                                        <input type="text" class="form-control" name="txt_link" placeholder="nhập đường dẫn">
                                    </div>
                                    <div class="form-group">
                                        <label >Trạng thái</label>
                                        <div id="status">
                                            <select name="slt_status" class="form-control">
                                                <option value="">--Chọn--</option>
                                                <option value="0">Ẩn</option>
                                                <option value="1">Hiển thị</option>
                                            </select>
                                        </div>
                                    </div>

                                </div>
                                <!-- /.card-body -->
                                <div class="card-footer">
                                    <button type="submit" class="btn btn-primary btn-add">Thêm</button>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="content-edit">
                        <div class="card card-primary">
                            <div class="card-header">
                                <h3 class="card-title">Sửa website</h3>
                            </div>
                            <!-- /.card-header -->
                            <!-- form start -->
                            <form method="Post"  role="form1" id="suasite_form">

                                <div class="card-body">
                                    @csrf
                                    <div class="err"></div>
                                    <input type="hidden" class="form-control" name="id" id="txt_id" placeholder="nhập tên loại sân">
                                    <div class="form-group">
                                        <label>Tên</label>
                                        <input type="text" class="form-control" name="txt_name" id="txt_name" placeholder="nhập tên loại sân" >
                                    </div>
                                    <div class="form-group">
                                        <label >Danh mục</label>
                                        <div class="cate">

                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label>Đường dẫn</label>
                                        <input type="text" class="form-control" name="txt_link" id="txt_link" placeholder="nhập đường dẫn" >
                                    </div>
                                    <div class="form-group">
                                        <label >Trạng thái</label>
                                        <div class="status">

                                        </div>
                                    </div>

                                </div>
                                <!-- /.card-body -->
                                <div class="card-footer">
                                    <button type="submit" class="btn btn-primary">Sửa</button>
                                    <button type="button" class="btn btn-warning btn-huy">Hủy</button>
                                </div>
                            </form>
                        </div>

                    </div>
                </div>
                <div class="col-9">
                    <dic class="content-site">
                        <div class="card card-warning">
                            <div class="card-header">
                                <h3 class="card-title">Danh sách website</h3>
                            </div>
                            <!-- /.card-header -->
                            <div class="card-body">
                                <div class="content-loai-san">
                                    <table class="table table-bordered">
                                        <tr>
                                            <th>STT</th>
                                            <th>Tên</th>
                                            <th>Đường dẫn</th>
                                            <th>Trạng thái</th>
                                            <th>Hành động</th>
                                        </tr>
                                        <?php $stt=0 ?>
                                        @foreach($site as $item)
                                            <tr>
                                                <td>{{++$stt}}</td>
                                                <td>{{$item->name}}</td>
                                                <td>{{$item->link}}</td>
                                                <td>@if($item->status==1)
                                                        {{"Hiện"}}
                                                    @else {{"Ẩn"}}
                                                    @endif
                                                </td>
                                                <td><button type="button" class="btn btn-success btn-del-site" id_site="{{$item->id}}">Xóa</button> <button class="btn btn-warning btn-edit-site" id_site="{{$item->id}}">Sửa</button></td>
                                            </tr>
                                        @endforeach
                                    </table>
                                </div>
                            </div>
                            <!-- /.card-body -->
                        </div>
                    </dic>
                    <!-- /.card -->
                </div>
                <!-- /.col -->
                <!-- /.col -->
            </div>
        </div>
    </section>

@endsection
