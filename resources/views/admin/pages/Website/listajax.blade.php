<div class="card card-warning">
    <div class="card-header">
        <h3 class="card-title">Danh sách website</h3>
    </div>
    <!-- /.card-header -->
    <div class="card-body">
        <div class="content-loai-san">
            <table class="table table-bordered">
                <tr>
                    <th>STT</th>
                    <th>Tên</th>
                    <th>Đường dẫn</th>
                    <th>Trạng thái</th>
                    <th>Hành động</th>
                </tr>
                <?php $stt=0 ?>
                @foreach($site as $item)
                    <tr>
                        <td>{{++$stt}}</td>
                        <td>{{$item->name}}</td>
                        <td>{{$item->link}}</td>
                        <td>@if($item->status==1)
                                {{"Hiện"}}
                            @else {{"Ẩn"}}
                            @endif
                        </td>
                        <td><button type="button" class="btn btn-success btn-del-site" id_site="{{$item->id}}">Xóa</button> <button class="btn btn-warning btn-edit-site" id_site="{{$item->id}}">Sửa</button></td>
                    </tr>
                @endforeach
            </table>
        </div>
    </div>
    <!-- /.card-body -->
</div>