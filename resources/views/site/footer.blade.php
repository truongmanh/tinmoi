<footer id="footer">
    <div class="footer_top">
        <div class="row">
            <div class="col-lg-4 col-md-4 col-sm-4">
                <div class="footer_widget wow fadeInLeftBig">
                    <h2>NewFeed</h2>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-4">
                <div class="footer_widget wow fadeInDown">
                    <h2>Danh mục</h2>
                    <ul class="tag_nav">
                        <li><a href="#">Sức khỏe</a></li>
                        <li><a href="#">Giải trí</a></li>
                        <li><a href="#">Thể thao</a></li>
                    </ul>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-4">
                <div class="footer_widget wow fadeInRightBig">
                    <h2>Liên hệ:</h2>
                    <p>Số 29 ,Phường Tân Thịnh, Tp.Thái Nguyên</p>
                </div>
            </div>
        </div>
    </div>
    <div class="footer_bottom">
        <p class="copyright">Copyright &copy; 2045 <a href="index.html">NewsFeed</a></p>
        <p class="developer">Developed By Wpfreeware</p>
    </div>
</footer>