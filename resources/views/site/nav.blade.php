<section id="navArea">
    <nav class="navbar navbar-inverse" role="navigation">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
            <ul class="nav navbar-nav main_nav">
                <li class="active"><a href="/"><span class="fa fa-home desktop-home"></span><span class="mobile-show">Trang chủ</span></a></li>
                <li><a href="{{route('tinmoi')}}">Sức khỏe</a></li>
                <li><a href="{{route('suckhoe')}}">Thể thao</a></li>
                <li><a href="{{route('giaitri')}}">Giải trí</a></li>
            </ul>
        </div>
    </nav>
</section>