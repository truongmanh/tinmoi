<!DOCTYPE html>
<html>
<head>
    <title>@yield('title')</title>
    <meta charset="utf-8">
    <base href="{{asset('')}}">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="site/assets/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="site/assets/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="site/assets/css/animate.css">
    <link rel="stylesheet" type="text/css" href="site/assets/css/font.css">
    <link rel="stylesheet" type="text/css" href="site/assets/css/li-scroller.css">
    <link rel="stylesheet" type="text/css" href="site/assets/css/slick.css">
    <link rel="stylesheet" type="text/css" href="site/assets/css/jquery.fancybox.css">
    <link rel="stylesheet" type="text/css" href="site/assets/css/theme.css">
    <link rel="stylesheet" type="text/css" href="site/assets/css/style.css?v=<?= time()?>">
    <!--[if lt IE 9]>
    <script src="site/assets/js/html5shiv.min.js"></script>
    <script src="site/assets/js/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<div id="preloader">
    <div id="status">&nbsp;</div>
</div>
<a class="scrollToTop" href="#"><i class="fa fa-angle-up"></i></a>
<div class="container">
    @include('site.header')
    @include('site.nav')
   @yield('content')
   @include('site.footer')
</div>
<script src="site/assets/js/jquery.min.js"></script>
<script src="site/assets/js/wow.min.js"></script>
<script src="site/assets/js/bootstrap.min.js"></script>
<script src="site/assets/js/slick.min.js"></script>
<script src="site/assets/js/jquery.li-scroller.1.0.js"></script>
<script src="site/assets/js/jquery.newsTicker.min.js"></script>
<script src="site/assets/js/jquery.fancybox.pack.js"></script>
<script src="site/assets/js/custom.js"></script>
</body>
</html>