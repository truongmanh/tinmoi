@extends('site.master')
@section('title','Trang chủ')
@section('content')
    <section id="newsSection">
        <div class="row">
            <div class="col-lg-12 col-md-12">
                <div class="latest_newsarea"> <span>Tin mới</span>
                    <ul id="ticker01" class="news_sticker">
                        @foreach($tinmoi_slider as $item)
                        <li><a href="#"><img src="{{$item->image}}" alt="">{{$item->title}}</a></li>
                        @endforeach
                    </ul>
                    <div class="social_area">
                        <ul class="social_nav">
                            <li class="facebook"><a href="#"></a></li>
                            <li class="twitter"><a href="#"></a></li>
                            <li class="googleplus"><a href="#"></a></li>
                            <li class="youtube"><a href="#"></a></li>
                            <li class="mail"><a href="#"></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section id="sliderSection">
        <div class="row">
            <div class="col-lg-8 col-md-8 col-sm-8">
                <div class="slick_slider">
                    @foreach($tinmoi_slider as $item)
                    <div class="single_iteam"> <a href="{{$item->link}}"> <img src="{{$item->image}}" alt=""></a>
                        <div class="slider_article">
                            <h2><a class="slider_tittle" href="pages/single_page.html">{{$item->title}}</a></h2>
                            <p>{!! substr($item->mota,0,110) !!}...</p>
                        </div>
                    </div>
                        @endforeach
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-4">
                <div class="latest_post">
                    <h2><span>Tin mới</span></h2>
                    <div class="latest_post_container">
                        <div id="prev-button"><i class="fa fa-chevron-up"></i></div>
                        <ul class="latest_postnav">
                            @foreach($tinmoi as $item)
                            <li>
                                <div class="media"> <a href="{{$item->link}}" class="media-left"> <img alt="" src="{{$item->image}}"> </a>
                                    <div class="media-body"> <a href="{{$item->link}}" class="catg_title">{{$item->title}}</a> </div>
                                </div>
                            </li>
                                @endforeach
                        </ul>
                        <div id="next-button"><i class="fa  fa-chevron-down"></i></div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section id="contentSection">
        <div class="row">
            <div class="col-lg-8 col-md-8 col-sm-8">
                <div class="left_content">
                    <div class="single_post_content">
                        <h2><span>Sức khỏe</span></h2>
                        <div class="single_post_content_left">
                            <ul class="business_catgnav  wow fadeInDown">
                                @foreach($suckhoe_title as $item)
                                <li>
                                    <figure class="bsbig_fig"> <a href="{{$item->link}}" class="featured_img"> <img alt="" src="{{$item->image}}"> <span class="overlay"></span> </a>
                                        <figcaption> <a href="{{$item->link}}">{{$item->title}}</a> </figcaption>
                                        <p>{!! substr($item->mota,0,110) !!}...</p>
                                    </figure>
                                </li>
                                    @endforeach
                            </ul>
                        </div>
                        <div class="single_post_content_right">
                            <ul class="spost_nav">
                                @foreach($suckhoe as $item)
                                <li>
                                    <div class="media wow fadeInDown"> <a href="{{$item->link}}" class="media-left"> <img alt="" src="{{$item->image}}"> </a>
                                        <div class="media-body"> <a href="{{$item->title}}" class="catg_title"> {{$item->title}}</a> </div>
                                    </div>
                                </li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                    <div class="fashion_technology_area">
                        <div class="fashion">
                            <div class="single_post_content">
                                <h2><span>Giải trí</span></h2>
                                <ul class="business_catgnav wow fadeInDown">
                                    @foreach($giaitri_title as $item)
                                        <li>
                                            <figure class="bsbig_fig"> <a href="{{$item->link}}" class="featured_img"> <img alt="" src="{{$item->image}}"> <span class="overlay"></span> </a>
                                                <figcaption> <a href="pages/single_page.html">{{$item->title}}</a> </figcaption>
                                                <p>{!! substr($item->mota,0,110) !!}...</p>
                                            </figure>
                                        </li>
                                    @endforeach
                                </ul>
                                <ul class="spost_nav">
                                    @foreach($giaitri as $item)
                                    <li>
                                        <div class="media wow fadeInDown"> <a href="{{$item->link}}" class="media-left"> <img alt="" src="{{$item->image}}"> </a>
                                            <div class="media-body"> <a href="{{$item->link}}" class="catg_title"> {{$item->title}}</a> </div>
                                        </div>
                                    </li>
                                        @endforeach
                                </ul>
                            </div>
                        </div>
                        <div class="technology">
                            <div class="single_post_content">
                                <h2><span>Thể thao</span></h2>
                                <ul class="business_catgnav">
                                    @foreach($thethao_title as $item)
                                        <li>
                                            <figure class="bsbig_fig"> <a href="{{$item->link}}" class="featured_img"> <img alt="" src="{{$item->image}}"> <span class="overlay"></span> </a>
                                                <figcaption> <a href="pages/single_page.html">{{$item->title}}</a> </figcaption>
                                                <p>{!! substr($item->mota,0,110) !!}...</p>
                                            </figure>
                                        </li>
                                    @endforeach
                                </ul>
                                <ul class="spost_nav">
                                    @foreach($thethao as $item)
                                        <li>
                                            <div class="media wow fadeInDown"> <a href="{{$item->link}}" class="media-left"> <img alt="" src="{{$item->image}}"> </a>
                                                <div class="media-body"> <a href="{{$item->link}}" class="catg_title"> {{$item->title}}</a> </div>
                                            </div>
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-4">
                <aside class="right_content">
                    <div class="single_sidebar">
                        <h2><span>Bài viết phổ biến</span></h2>
                        <ul class="spost_nav">
                            @foreach($tinnoibat as $item)
                            <li>
                                <div class="media wow fadeInDown"> <a href="{{$item->link}}" class="media-left"> <img alt="" src="{{$item->image}}"> </a>
                                    <div class="media-body"> <a href="{{$item->link}}" class="catg_title">{{$item->title}}</a> </div>
                                </div>
                            </li>
                                @endforeach
                        </ul>
                    </div>
                </aside>
            </div>
        </div>
    </section>
    @endsection