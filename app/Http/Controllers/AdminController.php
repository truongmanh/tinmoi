<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AdminController extends Controller
{
    public function index()
    {
        if(Auth::check()){
            return redirect()->route('dmdanhsach');
        }
        else{
            return view('admin.login');
        }
    }

    public function getLogin(){
        return view('admin.login');
    }

    public function postLogin(Request $request){
        $name=$request->Username;
        $password=$request->Password;

        if ( Auth::attempt(['name' => $name, 'password' =>$password])) {

            return redirect()->route('getdas');

        }
        else{
            echo"loi";
        }
    }
    public function getLogout(){
        Auth::logout();
        return redirect('ql_admin/login');
    }
}
