<?php

namespace App\Http\Controllers;

use App\Tin;
use Illuminate\Http\Request;
use XmlParser;

class HomeController extends Controller
{
    public function index(){
        $tinmoi_slider=Tin::orderBy('id','desc')->limit(5)->get();
        $tinmoi=Tin::where('cate_id',7)->orderBy('id','desc')->limit(5)->get();
        $tinnoibat=Tin::where('cate_id',8)->orderBy('id','desc')->limit(5)->get();
        $suckhoe=Tin::where('cate_id',6)->orderBy('id','desc')->limit(5)->get();
        $suckhoe_title=Tin::where('cate_id',6)->inRandomOrder()->limit(1)->get();
        $giaitri_title=Tin::where('cate_id',5)->inRandomOrder()->limit(1)->get();
        $giaitri=Tin::where('cate_id',5)->orderBy('id','desc')->limit(5)->get();
        $thethao_title=Tin::where('cate_id',9)->inRandomOrder()->limit(1)->get();
        $thethao=Tin::where('cate_id',9)->orderBy('id','desc')->limit(5)->get();
        return view('site.pages.trangchu',compact('tinnoibat','tinmoi','tinmoi_slider','suckhoe','suckhoe_title','giaitri','giaitri_title','thethao','thethao_title'));
    }

    public function getTinmoi(){
        $tinmoi_slider=Tin::orderBy('id','desc')->limit(5)->get();
        $tinmoi=Tin::where('cate_id',7)->orderBy('id','desc')->limit(5)->get();
        $tinnoibat=Tin::where('cate_id',8)->orderBy('id','desc')->limit(5)->get();
        $suckhoe=Tin::where('cate_id',6)->orderBy('id','desc')->paginate(10);
//        $suckhoe_title=Tin::where('cate_id',6)->inRandomOrder()->limit(1)->get();

        return view('site.pages.tinmoi',compact('tinnoibat','tinmoi','tinmoi_slider','suckhoe','suckhoe_title','giaitri','giaitri_title','thethao','thethao_title'));
    }
    public function getSuckhoe(){
        $tinmoi_slider=Tin::orderBy('id','desc')->limit(5)->get();
        $tinmoi=Tin::where('cate_id',7)->orderBy('id','desc')->limit(5)->get();
        $tinnoibat=Tin::where('cate_id',8)->orderBy('id','desc')->limit(5)->get();
        $thethao=Tin::where('cate_id',9)->orderBy('id','desc')->limit(5)->get();
        return view('site.pages.suckhoe',compact('tinnoibat','tinmoi','tinmoi_slider','suckhoe','suckhoe_title','giaitri','giaitri_title','thethao','thethao_title'));
    }
    public function getGiaitri(){
        $tinmoi_slider=Tin::orderBy('id','desc')->limit(5)->get();
        $tinmoi=Tin::where('cate_id',7)->orderBy('id','desc')->limit(5)->get();
        $tinnoibat=Tin::where('cate_id',8)->orderBy('id','desc')->limit(5)->get();
        $giaitri=Tin::where('cate_id',5)->orderBy('id','desc')->limit(5)->get();
        return view('site.pages.giaitri',compact('tinnoibat','tinmoi','tinmoi_slider','suckhoe','suckhoe_title','giaitri','giaitri_title','thethao','thethao_title'));
    }
}
