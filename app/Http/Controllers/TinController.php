<?php

namespace App\Http\Controllers;

use App\Category;
use App\Tin;
use App\Website;
use Illuminate\Http\Request;
use XmlParser;

class TinController extends Controller
{
    public function index()
    {
        $cate=Category::all();
        $tin=Tin::paginate(10);
        return view('admin.pages.new.list',compact('cate','tin'));
    }

    public function getCateSite(Request $request){
        $site=Website::where('cate_id',$request->id)->get();
        return response()->json($site);
    }

    public function listTin($tin){
        return view('admin.pages.new.listajax',compact('tin'));
    }
    public function getTin(Request $request)
    {
        $cate=$request->slt_cate;
        $link=$request->slt_site;
        $xml = XmlParser::load($link);
        $data = $xml->parse([
            'data' => ['uses' => 'channel.item[title,link,description,pubDate,image]'],
        ]);


        foreach ($data['data'] as $item){
            $tin=new Tin();
            $tin->title=$item['title'];
            $tin->link=$item['link'];
            $tin->cate_id= $cate;
            $tin->pubdate=$item['pubDate'];
            $tin->image=$item['image'];
            $tin->mota=$item['description'];
            $tin->save();
        }
        return $this->listTin($data['data']);
    }
}
