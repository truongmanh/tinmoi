<?php

namespace App\Http\Controllers;

use App\Category;
use App\Website;
use Illuminate\Http\Request;

class WebsiteController extends Controller
{
    public function index()
    {
        $site=Website::orderBy('id','desc')->get();
        $cate=Category::all();
        return view('admin.pages.website.list',compact('site','cate'));
    }

    public function getList()
    {
        $site=Website::orderBy('id','desc')->get();
        $cate=Category::all();
        return view('admin.pages.website.listajax',compact('site','cate'));
    }
    public function AddSite(Request $request)
    {
        try{
            $site=new Website();
            $site->name=$request->txt_name;
            $site->link=$request->txt_link;
            $site->status=$request->slt_status;
            $site->cate_id=$request->slt_cate;
            $site->save();
            return $this->getList();
        }
        catch(\Exception $e){
            return $e->getMessage();
        }
    }

    public function DelSite(Request $request)
    {
        $site=Website::find($request->id);
        $site->delete();
        return $this->getList();
    }

    public function GetSite(Request $request)
    {
        $site=Website::where('id',$request->id)->get();
        $cate=Category::all();
        return response()->json([
            'cate'=>$cate,
            'site'=>$site
        ]);
    }

    public function PostSite(Request $request)
    {
        try{
            $site=Website::find($request->id);
            $site->name=$request->txt_name;
            $site->link=$request->txt_link;
            $site->status=$request->slt_status;
            $site->cate_id=$request->slt_cate;
            $site->update();
            return $this->getList();
        }
        catch(\Exception $e){
            return $e->getMessage();
        }
    }



}
