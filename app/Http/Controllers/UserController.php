<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    public function index()
    {
        $user=User::OrderBy('id','desc')->get();
        return view('admin.pages.users.list',compact('user'));
   }

    public function getList()
    {
        $user=User::OrderBy('id','desc')->get();
        return view('admin.pages.users.listajax',compact('user'));
   }
   public function addUser(Request $request){

        $user=new User();
        $user->name=$request->txt_name;
        $user->email=$request->txt_email;
        $user->password=Hash::make($request->txt_mk);
        $user->save();
        return $this->getList();
   }

   public function delUser(Request $request){
       $user=User::find($request->id);
       $user->delete();
       return $this->getList();
   }
}
