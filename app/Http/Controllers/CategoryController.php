<?php

namespace App\Http\Controllers;

use App\Category;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    public function index()
    {
        $danhmuc=Category::orderBy('id','desc')->get();
        return view("admin.pages.Category.list",compact('danhmuc'));
    }

    public function getList()
    {
        $danhmuc=Category::orderBy('id','desc')->get();
        return view("admin.pages.Category.listajax",compact('danhmuc'));
    }

    public function AddCate(Request $request)
    {
        try{
            $cate=new Category();
            $cate->name=$request->txt_name;
            $cate->slug=str_slug($request->txt_name);
            $cate->status=$request->slt_status;
            $cate->save();
            return $this->getList();
        }
        catch (\Exception $e){
            return $e->getMessage();
        }
    }

    public function DelCate(Request $request)
    {
        $cate=Category::find($request->id);
        $cate->delete();
        return $this->getList();
    }

    public function GetCate(Request $request)
    {
        try{
            $cate=Category::where('id',$request->id)->get();
            return response()->json($cate);
        }
        catch (\Exception $e){
            return $e->getMessage();
        }
    }

    public function PostCate(Request $request)
    {
        try{
            $cate=Category::find($request->id);
            $cate->name=$request->txt_name;
            $cate->slug=str_slug($request->txt_name);
            $cate->status=$request->slt_status;
            $cate->update();
            return $this->getList();
        }
        catch (\Exception $e){
            return $e->getMessage();
        }
    }
}
