<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get("/","HomeController@index");
Route::get("suc-khoe",["as"=>'tinmoi',"uses"=>"HomeController@getTinmoi"]);
Route::get("the-thao",["as"=>'suckhoe',"uses"=>"HomeController@getSuckhoe"]);
Route::get("giai-tri",["as"=>'giaitri',"uses"=>"HomeController@getGiaitri"]);


Route::get('ql_admin/login',['as'=>'login','uses'=>'AdminController@getLogin']);
Route::post('ql_admin/postlogin',['as'=>'postlogin','uses'=>'AdminController@postLogin']);
Route::get('ql_admin/logout',['as'=>'getlogout','uses'=>'AdminController@getLogout']);
Route::group(['middleware'=>'auth'],function (){
Route::prefix('ql_admin')->group(function () {

    Route::get('/',["as"=>'getdas',"uses"=>"AdminController@index"]);
    Route::prefix('danh-muc')->group(function () {
        Route::get('danh-sach',["as"=>'dmdanhsach',"uses"=>"CategoryController@index"]);
        Route::post('them-danh-muc',["as"=>'themdanhmuc',"uses"=>"CategoryController@AddCate"]);
        Route::get('xoa-danh-muc',["as"=>'xoadanhmuc',"uses"=>"CategoryController@DelCate"]);
        Route::get('sua-danh-muc',["as"=>'suadanhmuc',"uses"=>"CategoryController@GetCate"]);
        Route::post('post-danh-muc',["as"=>'suadanhmuc',"uses"=>"CategoryController@PostCate"]);
    });
    Route::prefix('website')->group(function () {
        Route::get('danh-sach',["as"=>'sitedanhsach',"uses"=>"WebsiteController@index"]);
        Route::post('them-site',["as"=>'themsite',"uses"=>"WebsiteController@AddSite"]);
        Route::get('xoa-site',["as"=>'xoasite',"uses"=>"WebsiteController@DelSite"]);
        Route::get('sua-site',["as"=>'suasite',"uses"=>"WebsiteController@GetSite"]);
        Route::post('post-site',["as"=>'suasite',"uses"=>"WebsiteController@PostSite"]);
    });
    Route::prefix('tin')->group(function () {
        Route::get('get-tin',["as"=>'gettin',"uses"=>"TinController@index"]);
        Route::get('get-site',["as"=>'getcatesite',"uses"=>"TinController@getCateSite"]);
        Route::post('get-tin-moi',["as"=>'gettinmoi',"uses"=>"TinController@getTin"]);
    });
    Route::prefix('users')->group(function () {
        Route::get('danh-sach',["as"=>'udanhsach',"uses"=>"UserController@index"]);
        Route::post('add-user',["as"=>'adduser',"uses"=>"UserController@addUser"]);
        Route::get('del-user',["as"=>'deluser',"uses"=>"UserController@delUser"]);

    });
});
});