//danhmuc
$(document).ready(function(){
    // jQuery methods go here...
    $("#danhmuc_form").validate({
       rules:{
            txt_name:"required",
            slt_status:"required"
       } ,
        messages:{
            txt_name:"Tên không được để trống",
            slt_status:"Trạng thái không được để trống"
        },
        submitHandler: function(form) {
            themdanhmuc();
        }
    });

    $("#suadanhmuc_form").validate({
        rules:{
            txt_name:"required",
            slt_status:"required"
        } ,
        messages:{
            txt_name:"Tên không được để trống",
            slt_status:"Trạng thái không được để trống"
        },
        submitHandler: function(form) {
            suadanhmuc();
        }
    });

    // thêm
  function themdanhmuc() {
      var data=$("#danhmuc_form").serialize();
      $.ajax({
          url:"ql_admin/danh-muc/them-danh-muc",
          type:"POST",
          data:data,
          dataType:"html"
      }).done(function (data) {
          $(".content-danhmuc").html(data);
          $("#danhmuc_form").trigger('reset');
          // console.log(data);
      }).fail(function (erro) {
        console.log(erro);
      });
  }

    // sửa
   function suadanhmuc() {
       var data=$("#suadanhmuc_form").serialize();
       // console.log(data);
       $.ajax({
           url:"ql_admin/danh-muc/post-danh-muc",
           type:"POST",
           data:data,
           dataType:"html"
       }).done(function (data) {

           $(".content-danhmuc").html(data);
           $("#suadanhmuc_form").trigger('reset');

       }).fail(function (erro) {
          console.log(erro);
       });
   }


    $("body").on('click','.btn-del-dm',function (e) {
        e.preventDefault();
        var id=$(this).attr('id_dm');

        var checkdel=confirm("Bạn có muốn xóa không?");
        if(checkdel){
            $.ajax({
                url:"ql_admin/danh-muc/xoa-danh-muc",
                type:"GET",
                data:{id:id},
                dataType:"html"
            }).done(function (data) {
                $(".content-danhmuc").html(data);
                // console.log(data);
            }).fail(function (erro) {
                console.log(erro);
            });
        }
        else{return false;}
    });


    $("body").on('click','.btn-edit-dm',function (e) {
        e.preventDefault();
        var ids=$(this).attr('id_dm');
        $(".content-add").css("display","none");


        $.ajax({
            url:"ql_admin/danh-muc/sua-danh-muc",
            type:"GET",
            data:{id:ids},
            dataType:"json"
        }).done(function (data) {
            $("#txt_id").val(data[0].id);
            $("#txt_name").val(data[0].name);
            // $("#txt_gia").val(data[0].gia);
            if(data[0].status==0){
                sl=" <select name=\"slt_status\" class=\"form-control\">\n" +
                    "                                               <option value=\"0\" selected>Ẩn</option>\n" +
                    "                                               <option value=\"1\">Hiển thị</option>\n" +
                    "                                           </select>";
            }
            else{
                sl=" <select name=\"slt_status\" class=\"form-control\">\n" +
                    "                                               <option value=\"0\">Ẩn</option>\n" +
                    "                                               <option value=\"1\" selected>Hiển thị</option>\n" +
                    "                                           </select>";
            }
            $(".status").html(sl);
            $(".content-edit").css("display","block");
            // console.log(data);
        }).fail(function (erro) {
            console.log(erro);
        });
    });

    $("body").on('click','.btn-huy',function (e) {
        e.preventDefault();
        $(".content-add").css("display","block");
        $(".content-edit").css("display","none");
    });


    //website

      $("#site_form").validate({
          rules:{
              txt_name:"required",
              txt_link:"required",
              slt_cate:"required",
              slt_status:"required"
          },
          messages:{
              txt_name:"Tên không được để trống",
              txt_link:"Đường dẫn không được để trống",
              slt_status:"Trạng thái không được để trống",
              slt_cate:"Danh mục không được để trống"
          },
          submitHandler: function(form) {
              themsite();
          }
      });

    $("#suasite_form").validate({
        rules:{
            txt_name:"required",
            txt_link:"required",
            slt_cate:"required",
            slt_status:"required"
        },
        messages:{
            txt_name:"Tên không được để trống",
            txt_link:"Đường dẫn không được để trống",
            slt_status:"Trạng thái không được để trống",
            slt_cate:"Danh mục không được để trống"
        },
        submitHandler: function(form) {
            suasite();
        }
    });


    function themsite() {
        var data=$("#site_form").serialize();
        // console.log(data);
        $.ajax({
            url:"ql_admin/website/them-site",
            type:"POST",
            data:data,
            dataType:"html"
        }).done(function(data){
            $(".content-site").html(data);
            $("#site_form").trigger('reset');
        }).fail(function(erro){
            console.log(erro)
        });
    }

    function suasite() {
        var data=$("#suasite_form").serialize();
        // console.log(data);
        $.ajax({
            url:"ql_admin/website/post-site",
            type:"POST",
            data:data,
            dataType:"html"
        }).done(function(data){
            $(".content-site").html(data);
            $("#suasite_form").trigger('reset');
        }).fail(function(erro){
            console.log(erro)
        });
    }

    $("body").on("click",'.btn-del-site',function(e){
        e.preventDefault();
        var id=$(this).attr('id_site');
        // alert(id);
      var checkdel=confirm("Bạn có muốn xóa không?");
      if(checkdel){
          $.ajax({
              url:"ql_admin/website/xoa-site",
              type:"GET",
              data:{id:id},
              dataType:"html"
          }).done(function(data){
              $(".content-site").html(data);
          }).fail(function(erro){
              console.log(erro)
          });
      }
      else{
          return false;
      }
    });
//


$("body").on("click",".btn-edit-site",function (e) {
    e.preventDefault();
    var ids=$(this).attr('id_site');
    $(".content-add").css("display","none");

    $.ajax({
        url:"ql_admin/website/sua-site",
        type:"GET",
        data:{id:ids},
        dataType:"json"
    }).done(function (data) {

        $("#txt_id").val(data.site[0].id);
        $("#txt_name").val(data.site[0].name);
        $("#txt_link").val(data.site[0].link);
        // $("#txt_gia").val(data[0].gia);
        if(data.site[0].status==0){
            sl=" <select name=\"slt_status\" class=\"form-control\">\n" +
                "                                               <option value=\"0\" selected>Ẩn</option>\n" +
                "                                               <option value=\"1\">Hiển thị</option>\n" +
                "                                           </select>";
        }
        else{
            sl=" <select name=\"slt_status\" class=\"form-control\">\n" +
                "                                               <option value=\"0\">Ẩn</option>\n" +
                "                                               <option value=\"1\" selected>Hiển thị</option>\n" +
                "                                           </select>";
        }
        $(".status").html(sl);
        slc="<select name=\"slt_cate\" class=\"form-control\">";
        $.each(data.cate,function (i,n) {
            if(n.id==data.site[0].cate_id){
                slc+=" <option value="+ n.id+" selected>"+ n.name+"</option>";
            }
            slc+=" <option value="+ n.id+">"+ n.name+"</option>";

        });
        slc+="</select>";
        $(".cate").html(slc);
        $(".content-edit").css("display","block");
        // console.log(data);
    }).fail(function (erro) {
        console.log(erro);
    });
})


$("body").on("change","#slt_cate",function () {
 var cate=$(this).val();
    $.ajax({
        url:"ql_admin/tin/get-site",
        type:"GET",
        data:{id:cate},
        dataType:"json"
    }).done(function(data){
        var sl=" <select name=\"slt_site\" class=\"form-control\">";
        sl+="<option value=\"\">--Chọn--</option>";
        $.each(data,function (i,n) {

           sl+="<option value="+n.link+">"+n.name+"</option>" ;
        });
        sl+="</select>";
        $("#website").html(sl);
        // console.log(data);
    }).fail(function(erro){
        console.log(erro)
    });
});


$("body").on("click",".btn-lay-tin",function (e) {
   e.preventDefault();
   var data=$("#tin_form").serialize();

    $.ajax({
        url:"ql_admin/tin/get-tin-moi",
        type:"POST",
        data:data,
        dataType:"html"
    }).done(function(data){
        $(".content-site").html(data);
      console.log(data);
    }).fail(function(erro){
        console.log(erro)
    });
});
  $("body").on("click",".btn-share-fb",function (e) {
     e.preventDefault();
     var id=$(this).attr('id_tin');
     alert(id);
  });

  //user

    $("#user_form").validate({
        rules:{
            txt_name:"required",
            txt_email:"required",
            txt_mk:"required",
        },
        messages:{
            txt_name:"Tên không được để trống",
            txt_email:"Email không được để trống",
            txt_mk:"Mật khẩu không được để trống",
        },
        submitHandler: function(form) {
            adduser();
        }
    });

  function adduser() {
      var data=$("#user_form").serialize();
      // console.log(data);
      $.ajax({
          url:"ql_admin/users/add-user",
          type:"POST",
          data:data,
          dataType:"html"
      }).done(function(data){
          $(".content-user").html(data);
          $("#user_form").trigger('reset');
      }).fail(function(erro){
          console.log(erro)
      });
  }

  $("body").on("click",".btn-del-user",function (e) {
      e.preventDefault();
      var id=$(this).attr("id_user");
      var checkdel=confirm("Bạn có muốn xóa không?");
      if(checkdel){
          $.ajax({
              url:"ql_admin/users/del-user",
              type:"GET",
              data:{id:id},
              dataType:"html"
          }).done(function(data){
              $(".content-user").html(data);
              $("#user_form").trigger('reset');
          }).fail(function(erro){
              console.log(erro)
          });
      }
    else{
          return false;
      }
  })
});



